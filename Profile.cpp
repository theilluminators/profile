#include "ProfileDeclaration.h"

Profile::Profile()//default constructor sets up a default profile
{
    numThresholds = 2;
    Name = "Batman";
    Password = "batman";
    ThresholdStart.push_back(0);
    ThresholdStart.push_back(50);
    ThresholdEnd.push_back(49);
    ThresholdEnd.push_back(100);
    colors.push_back("Green");
    colors.push_back("Red");

}

Profile::Profile(string name, string pass, int numThreshIn, vector<int> ThreshStartingIn, vector<int> ThreshEndingIn, vector<string> ColorsIn)	//Constructor function creates the Profile class with parameters
{

    numThresholds = numThreshIn;				//Populates private variables of new Profile class with the input parameters
    int tempInt;
    string tempString;

    Name = name;
    Password = pass;
    for (int i = 0; i < ThreshStartingIn.size(); i++)
    {
        tempInt = ThreshStartingIn[i];
        ThresholdStart.push_back(tempInt);
        tempInt = ThreshEndingIn[i];
        ThresholdEnd.push_back(tempInt);
    }

    for (int i = 0; i < ColorsIn.size(); i++)
    {
        tempString = ColorsIn[i];
        colors.push_back(tempString);
    }
}

void Profile::set_numThresholds(int num)
{
    numThresholds = num;
}

void Profile::set_password(string StrIn)
{
    Password = StrIn;
}

int Profile::get_ThreshStart(int num)	//member function that returns the starting threshold percentage at given threshold divison
{
    return ThresholdStart[num];
}

int Profile::get_ThreshEnd(int num)
{
    return ThresholdEnd[num];			//Returns the ending threshold percentage at given threshold division
}

int Profile::get_NumThresholds()
{
    return numThresholds;			//Returns private variable holding the number of thresholds
}

string Profile::get_color(int threshold)
{											//returns the color at specified threshold divison
    return colors[threshold];
}

string Profile::get_name()
{
    return Name;
}

string Profile::get_password()
{
    return Password;
}

void Profile::set_name(string StrIn)
{
    Name = StrIn;
}
vector<string> Profile::get_recentExes()		//Return the recentExes private variable
{
    return recentExes;
}
void Profile::appendToRecentExes(string Strin)			//Adds the string input variable to the private vector of recent exe paths
{
    recentExes.push_back(Strin);
}
