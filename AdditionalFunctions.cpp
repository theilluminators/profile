//Written By: Brandon Busuttil
//Date: 4/15/2015
//Description: Source file for many functions necessary for profile database interactions when using the Illumunator software
//Modified By: Suvro Sudip (updated syncing profile functions)


#include<string>
#include<vector>
#include<fstream>
#include<iostream>
#include "ProfileDeclaration.h"

#include <windows.h>
#include <wininet.h>
#pragma comment(lib, "wininet")

using namespace std;

//Necessarry External Variables
string filename = "database.txt";

/* Sync Profile Functions */
// Function to upload the database file to the FTP Server
void uploadFile() 
{
	HINTERNET hInternet; // Initiating an internet connection
	HINTERNET hFtpSession; // Initiating a FTP Session
	hInternet = InternetOpen(NULL, INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0); // Establishing an internet connection
	hFtpSession = InternetConnect(hInternet, "ftp.noramovins.com", INTERNET_DEFAULT_FTP_PORT, "noramovi", "@A1b1c1d1", INTERNET_SERVICE_FTP, 0, 0); // Connecting to FTP using FTP username, password, ftp server name
	
	// Logic to determine if ftp file transfer was successful
	if (FtpPutFile(hFtpSession, "database.txt", "database.txt", FTP_TRANSFER_TYPE_BINARY, 0)){
		MessageBox(NULL, "Successfully updated database in ftp server!", "Ftp Upload", NULL);
	}
	else{
		MessageBox(NULL, "Couldn't upload database to ftp server!", "Ftp Upload", NULL);
	}
	
	// Closing the established FTP and Internet sessions
	InternetCloseHandle(hFtpSession);
	InternetCloseHandle(hInternet);

}

// Function to download the database file from the FTP Server
void downloadFile()
{
	HINTERNET hInternet; // Initiating an internet connection
	HINTERNET hFtpSession; // Initiating a FTP Session
	hInternet = InternetOpen(NULL, INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0); // Establishing an internet connection 
	hFtpSession = InternetConnect(hInternet, "ftp.noramovins.com", INTERNET_DEFAULT_FTP_PORT, "noramovi", "@A1b1c1d1", INTERNET_SERVICE_FTP, 0, 0);  // Connecting to FTP using FTP username, password, ftp server name
	
	// Logic to determine if ftp file download was successful
	if (FtpGetFile(hFtpSession, "database.txt", "database.txt", FTP_TRANSFER_TYPE_BINARY,0,0,0)){
		MessageBox(NULL, "Successfully downloaded database from ftp server!", "Ftp Upload", NULL);
	}
	else{
		MessageBox(NULL, "Couldn't download database from ftp server! Sync Failed", "Ftp Upload", NULL);
	}
	
	// Closing the established FTP and Internet sessions
	InternetCloseHandle(hFtpSession);
	InternetCloseHandle(hInternet);

}

/* Sync Profile Functions End */


vector<string> GetProfiles()	//Open text file, and parse it while storing all names into a return vector, names must be the first string of every line
{
	// Download FTP Databse
	// downloadFile();
	
	ifstream infile;
	vector<string> names;

	infile.open(filename);

	if (infile.fail())			//If the file is not open, return an empty vector
		return names;

	while (!infile.eof())
	{
		string temp;
		char temp2 = ' ';
	

		infile >> temp;				//read in name from text file
		names.push_back(temp);		//push name into return vector
		getline(infile, temp);

		

	}

	infile.close();
	return names;
}

bool CheckPassword(string name, string password)		//scans the text file for a match to the input name and password 
{
	ifstream infile;
	string temp;


	infile.open(filename);

	if (infile.fail())						//check for a successful file open
		return false;

	while (!infile.eof())					//parse the entire text file
	{
		infile >> temp;						//read in one string at a time and compare to the input name
		if (temp == name)					//if there is a name match, proceed to read in the next available string
		{
			infile >> temp;					//the next available string will always be the user's password
			if (temp == password)			//if there is a match, it is a correct login
			{
				infile.close();
				return true;
			}

		}
	}

	infile.close();
	return false;							//if the loop finds end of file before a match is made, then false credentials
}

string SaveProfile(Profile newProf)	//sends in a profile and appends it to external text file and returns a status text of the save attempt
{
	ofstream outfile;
	ifstream infile;
	string temp = "test";
	int num;

	outfile.open(filename, std::ios_base::app);		//This outfile open opens the file, and automatically points to the end of the file for writing

	if (outfile.fail())
		return "Failure To Open Database File";		//Check for valid file open, return failure message

	temp = newProf.get_name();						//Extract variables and append them to the bottom of the text file
	outfile << temp << " ";
	
	temp = newProf.get_password();
	outfile << temp << " ";

	num = newProf.get_NumThresholds();
	outfile << num << " ";

	for (int i = 0; i < newProf.get_NumThresholds(); i++)
	{
		num = newProf.get_ThreshStart(i);			//the outputs starting percent
		outfile << num << " ";				
		num = newProf.get_ThreshEnd(i);				//followed by that thresholds ending percent
		outfile << num << " ";
		temp = newProf.get_color(i);				//followed by the color that represents that threshold division
		outfile << temp << " ";
	}
	outfile << endl;								//Start a new line for any new saves
	outfile.close();
	
	// Upload database to FTP Server
	// uploadFile();
	
	return "Save Successful";
}


Profile GetProfile(string name)				//Parses the text file until it finds a name that matches, and loads the profile with the specified name
{
	ifstream infile;
	string tempName;
	string tempPassword;
	string tempColor;
	int numThresh;
	int temp;
	vector<int> ThreshStart;
	vector<int> ThreshEnd;
	vector<string> color;


	infile.open(filename);

	while (!infile.eof())					//Reach the line of the file with the user profile
	{
		infile >> tempName;
		if (tempName == name)
			break;
	}

	infile >> tempPassword; 				//Read the password in
	infile >> numThresh;					//Read number of thresholds divisions in

	for (int i = 0; i < numThresh; i++)
	{
		infile >> temp;						//Read in starting percent for threshold
		ThreshStart.push_back(temp);		//Push start value into vector
		infile >> temp;						//Read end percent for threshold
		ThreshEnd.push_back(temp);			//Push end value into vector
		infile >> tempColor;				//Read in color for corresponding threshold
		color.push_back(tempColor);			//Push color into color vector
	}

	Profile newProf(tempName,tempPassword,numThresh, ThreshStart, ThreshEnd, color);	//Generate profile class
	
	return newProf;
}

